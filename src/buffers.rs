#![feature(slice_patterns)]

//! A module for manipulating hex and base64

pub use self::base64::buffer_to_base64;

mod base64;

/// Convert a string of hex characters to a buffer
pub fn hex_string_to_buffer(hex: &str) -> Vec<u8> {
    hex.chars().collect::<Vec<_>>().chunks(2).map(hex_pair_to_int).collect()
}

pub fn buffer_to_hex_string(buffer: &Vec<u8>) -> String {
    buffer.into_iter().flat_map(int_to_hex_pair).collect()
}

pub fn xor(buffer1: &Vec<u8>, buffer2: &Vec<u8>) -> Vec<u8> {
    buffer1.into_iter().zip(buffer2).map(|(a,b)| a ^ b).collect()
}

fn hex_pair_to_int(pair: &[char]) -> u8 {
    hex_to_int(pair[0]) * 16 + hex_to_int(pair[1])
}

/// Convert a single hex character (0-9, a-f) into the integer it represents
fn hex_to_int(hex: char) -> u8 {
    match hex {
        '0'...'9' => hex as u8 - '0' as u8,
        'a'...'f' => hex as u8 - 'a' as u8 + 10,
        _ => panic!("Character {} out of range", hex)
    }
}

fn int_to_hex_pair(number: &u8) -> Vec<char> {
    vec![int_to_hex(number >> 4), int_to_hex(number & 0x0f)]
}

fn int_to_hex(half: u8) -> char {
    match half {
        0...9 => (half + '0' as u8) as char,
        10...15 => (half - 10 + 'a' as u8) as char,
        _ => panic!("Number {} out of range", half)
    }
}

#[test]
fn all_hexes() {
    let hexes = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'];
    for i in 0usize..hexes.len() {
        assert_eq!(hex_to_int(hexes[i]), i as u8);
    }
}

#[test]
#[should_panic]
fn test_too_large() {
    hex_to_int('g');
}

#[test]
#[should_panic]
fn test_too_small() {
    hex_to_int('/');
}

#[test]
fn test_string_to_buffer() {
    assert_eq!(hex_string_to_buffer("0a"), vec![10]);
    assert_eq!(hex_string_to_buffer("05af"), vec![5, 175]);
}

#[test]
fn test_buffer_to_hex_string() {
    assert_eq!(buffer_to_hex_string(&vec![10]), "0a");
    assert_eq!(buffer_to_hex_string(&vec![5, 175]), "05af");
}

#[test]
fn test_xor() {
    let a = hex_string_to_buffer("1c0111001f010100061a024b53535009181c");
    let b = hex_string_to_buffer("686974207468652062756c6c277320657965");

    let result = xor(&a, &b);

    assert_eq!(buffer_to_hex_string(&result), "746865206b696420646f6e277420706c6179");
}
