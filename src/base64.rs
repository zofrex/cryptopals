/// Convert a buffer to its base64 representation
pub fn buffer_to_base64(buffer: &Vec<u8>) -> String {
    let mut base64: Vec<char> = vec![];
    for group in buffer.chunks(3) {
        let res = match group {
            [a] => int_singlet_to_base64(&[a]),
            [a, b] => int_couplet_to_base64(&[a, b]),
            [a, b, c] => int_triplet_to_base64(&[a, b, c]),
            _ => panic!()
        };
        base64.extend(res);
    }
    base64.into_iter().collect()
}

/// Look up mappings of base64 values to characters
fn index_base64(index: u8) -> char {
    if index < 26 {
        ('A' as u8 + index) as char
    } else if index < 52 {
        ('a' as u8 + index - 26) as char
    } else if index < 62 {
        ('0' as u8 + index - 52) as char
    } else if index == 62 {
        '+'
    } else if index == 63 {
        '/'
    } else {
        panic!("Index {} out of range", index);
    }
}

/// Convert three integers into four base64'd characters
fn int_triplet_to_base64(triplet: &[u8; 3]) -> Vec<char> {
    let mut indexes: [u8; 4] = [0,0,0,0];
    indexes[0] = (triplet[0] & 0b11111100) >> 2;
    indexes[1] = ((triplet[0] & 0b00000011) << 4) | ((triplet[1] & 0b11110000) >> 4);
    indexes[2] = ((triplet[1] & 0b00001111) << 2) | ((triplet[2] & 0b11000000) >> 6);
    indexes[3] = triplet[2] & 0b00111111;
    indexes.iter().map(|i| index_base64(*i)).collect()
}

/// Convert three integers into four base64'd characters
fn int_couplet_to_base64(couplet: &[u8; 2]) -> Vec<char> {
    let mut result = int_triplet_to_base64(&[couplet[0], couplet[1], 0]);
    result[3] = '=';
    result
}

/// Convert three integers into four base64'd characters
fn int_singlet_to_base64(singlet: &[u8; 1]) -> Vec<char> {
    let mut result = int_couplet_to_base64(&[singlet[0], 0]);
    result[2] = '=';
    result
}

#[test]
fn test_buffer_to_base64() {
    let buffer = vec![77, 97, 110];
    assert_eq!(buffer_to_base64(&buffer), "TWFu");
}

#[test]
fn test_buffer_to_base64_1_padding() {
    let buffer = vec![77, 97];
    assert_eq!(buffer_to_base64(&buffer), "TWE=");
}

#[test]
fn test_buffer_to_base64_2_padding() {
    let buffer = vec![77];
    assert_eq!(buffer_to_base64(&buffer), "TQ==");
}

#[test]
fn look_up_base64_index_caps() {
    let base64s = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    for i in 0usize..base64s.len() {
        assert_eq!(index_base64(i as u8), base64s[i]);
    }
}

#[test]
fn look_up_base64_index_lowers() {
    let base64s = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    for i in 0usize..base64s.len() {
        assert_eq!(index_base64(i as u8 + 26), base64s[i]);
    }
}

#[test]
fn look_up_base64_index_numbers() {
    let numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    for i in 0..numbers.len() {
        assert_eq!(index_base64(i as u8 + 52), numbers[i]);
    }
}

#[test]
fn look_up_base64_others() {
    assert_eq!(index_base64(62), '+');
    assert_eq!(index_base64(63), '/');
}

#[test]
#[should_panic]
fn look_up_base64_too_large() {
    index_base64(64);
}

#[test]
fn test_triplet_to_base64() {
    assert_eq!(int_triplet_to_base64(&[77, 97, 110]), vec!['T', 'W', 'F', 'u']);
}

#[test]
fn test_couplet_to_base64() {
    assert_eq!(int_couplet_to_base64(&[77, 97]), vec!['T', 'W', 'E', '=']);
}

#[test]
fn test_singlet_to_base64() {
    assert_eq!(int_singlet_to_base64(&[77]), vec!['T', 'Q', '=', '=']);
}
