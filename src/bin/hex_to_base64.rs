extern crate buffers;

/// Interactive solution for Set 1 Challenge 1: Convert hex to base64.
///
/// Reads hex-encoded input from stdin and output hex-encoded base64 representation.
#[cfg(not(test))]
fn main() {
    let mut input = String::new();
    std::io::stdin().read_line(&mut input).expect("Failed to read line");
    let input = input.trim();
    println!("Output: {:?}", hex_to_base64(&input));
}

fn hex_to_base64(hex: &str) -> String {
    buffers::buffer_to_base64(&buffers::hex_string_to_buffer(hex))
}

#[test]
fn test_hex_to_base64_cryptopals_1_1() {
    let input = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";
    let expected = "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t";

    let result = hex_to_base64(&input);

    assert_eq!(result, expected);
}
